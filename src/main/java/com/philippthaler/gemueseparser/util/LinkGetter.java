package com.philippthaler.gemueseparser.util;

import java.io.IOException;
import java.util.Map;

public interface LinkGetter {

  Map<String, String> getLinks() throws IOException;
}
