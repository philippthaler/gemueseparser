package com.philippthaler.gemueseparser.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KeywordGenerator {

  private Set<String> keywords;

  private String[] noodleSynonyms = {
      "pasta",
      "nudel",
      "penne",
      "spaghetti"
  };

  private String[] soupSynonyms = {
      "suppe",
      "eintopf"
  };

  public Set<String> generateKeywords(String text) {
    keywords = new HashSet<>();
    addUppercaseWords(text);
    addSynonyms(text, noodleSynonyms, "Nudeln");
    addSynonyms(text, soupSynonyms, "Suppe");

    return keywords;
  }

  private void addUppercaseWords(String text) {
    String[] words = text.split("[ \\-–]");
    for (int i = 0; i < words.length; i++) {
      try {
        String word = words[i];
        word = word.replaceAll("[^\\dA-Za-züäöÜÄÖ ]", "").replaceAll("\\s+", "+");
        if (Character.isUpperCase(word.charAt(0))) {
          keywords.add(word);
        }
      } catch (StringIndexOutOfBoundsException ignored) {

      }
    }
  }

  private void addSynonyms(String text, String[] words, String synonym) {
    if(StringUtils.stringContainsItemFromList(text, words)) {
      keywords.add(synonym);
    }
  }

}




