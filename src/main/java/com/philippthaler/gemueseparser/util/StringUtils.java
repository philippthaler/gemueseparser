package com.philippthaler.gemueseparser.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtils {

  /**
   * Takes a string and checks if it contains one of many words provided by an array
   * @param inputStr The string to check if it contains those words provided
   * @param items The words to check
   * @return
   */
  public static boolean stringContainsItemFromList(String inputStr, String[] items) {
    return Arrays.stream(items).parallel().anyMatch(inputStr::contains);
  }

  /**
   * Fixes the multiline ingredients and puts them all in the same field seperated by [br]
   * @param array the array to fix
   * @return the fixed array
   */
  public static String[] fixMultiLineIngredients(String[] array) {
    StringBuilder stringBuilder = new StringBuilder();
    int startingIndex = StringUtils.stringContainsItemFromList(array[1].toLowerCase(), new String[]{"person", "rezept", "blech"}) ? 2 : 1;
    stringBuilder.append(array[startingIndex]);
    for (int i = startingIndex; i < array.length; i++) {
      // Checks if the element has text like "Fülle:", if there are more than 3 digits and it doesn't contain the word "minute"
      // to check if it's a multiline ingredient
      if (array[i].matches("(.*)[a-zA-Z]:(.*)") && array[i].replaceAll("\\D","").length() > 3 && !stringContainsItemFromList(array[i].toLowerCase(), new String[] {"minute", "°","backen"}))  {
        stringBuilder.append(",").append(array[i].replaceAll(":[ ]*", ":,"));
        array[i] = "";
      }
    }
    array[startingIndex] = stringBuilder.toString();
    List<String> tempArray = new ArrayList<>();
    for (int i = 0; i < array.length; i++) {
      if(!array[i].equals("")) {
        tempArray.add(array[i]);
      }
    }
    return tempArray.toArray(new String[0]);
  }
}
