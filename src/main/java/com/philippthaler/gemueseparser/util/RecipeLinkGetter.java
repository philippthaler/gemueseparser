package com.philippthaler.gemueseparser.util;

import com.philippthaler.gemueseparser.util.LinkGetter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RecipeLinkGetter implements LinkGetter {

  private final Document document;

  private final Map<String, String> categoryLinks;

  public RecipeLinkGetter(String url) throws IOException {
    document = Jsoup.connect(url).get();
    categoryLinks = new HashMap<>();
  }

  /**
   * @return Map<String , String>, with the categories as the key and the URLs as the value
   * @throws IOException
   */
  public Map<String, String> getLinks() throws IOException {
    // Gets the div around the category links
    Elements linkWrapper = document.select("#item10");
    // Gets all category links
    Elements links = linkWrapper.select("a[href]");

    // Saves the category names as the key, the url as the value
    // e.g. Sprossenkohl: https://tiroler.gemuesekiste.at/rezeptesprossenk.html
    for (Element link : links) {
      categoryLinks.put(link.text(), link.attr("abs:href"));
    }

    return categoryLinks;
  }
}
