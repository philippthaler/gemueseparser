package com.philippthaler.gemueseparser.parsing;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;

import java.util.List;
import java.util.Map;

public interface RecipeParser {
  /**
   * Method that parses a HTML page and returns recipes as a List of a Map
   * A single recipe is represented as a Map<String, String>, for example: ("Title", "Cheeseburger"), ("Ingredients", "Meat, Bread, Onions,...")
   * @return The list of recipes
   */
  List<Map<String, String>> parse();

  /**
   * Method that cleans the HTML by removing span, a, and em tags
   *
   * @param element The element that's been cleaned
   * @return The cleaned HTML
   */
  default String cleanHTML(Element element, Whitelist argument) {
    return Jsoup.clean(element.toString(), argument);
  }
}
