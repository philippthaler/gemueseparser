package com.philippthaler.gemueseparser.parsing;

import com.philippthaler.gemueseparser.parsing.strategy.*;
import com.philippthaler.gemueseparser.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class GemueseParser implements RecipeParser {

    private Document document;

    /**
     * Constructor for parsing recipes for Tiroler Gemüsekiste
     *
     * @param url the url
     * @throws IOException
     */
    public GemueseParser(String url) throws IOException {
        document = Jsoup.connect(url).get();
        document = Jsoup.parse(cleanHTML(document.body(), Whitelist.basic().removeTags("span", "a", "em", "strong")));
    }

    /**
     * Constructor for parsing recipes for Tiroler Gemüsekiste
     *
     * @param url The url
     * @param id  The id, where the recipes are on the page. Used for the main recipes
     * @throws IOException
     */
    public GemueseParser(String url, String id) throws IOException {
        document = Jsoup.connect(url).get();
        Element recipeWrapper = document.getElementById(id);
        document = Jsoup.parse(cleanHTML(recipeWrapper, Whitelist.basic().removeTags("span", "a", "em", "strong")));

    }

    /**
     * Parses the document and creates a List of Recipes, where 1 recipe is a Map<String,String>
     *
     * @return All recipes found on the url
     */
    @Override
    public List<Map<String, String>> parse() {
        // Select all <p> tags, where the recipes are written
        Elements recipeElements = document.select("p");

        List<Map<String, String>> recipes = new ArrayList<>();
        ParsingStrategy parsingStrategy;
        for (Element recipeElement : recipeElements) {
            String[] recipeParts = recipeElement.html().split("<br>|\u2028");
            String lowCarb = recipeElement.toString().toLowerCase().contains("low carb") ? "on" : "off";
            if (recipeParts.length > 1) {
                recipeParts = StringUtils.fixMultiLineIngredients(recipeParts);

                // Removes the Low Carb text from the title if it's there
                if (recipeParts[0].toLowerCase().contains("low carb")) {
                    recipeParts[0] = recipeParts[0].substring(0, recipeParts[0].toLowerCase().indexOf("low carb") - 3);
                }
                if (recipeParts[1].toLowerCase().contains("low carb")) {
                    parsingStrategy = new LCSecondLineParsingStrategy();
                } else if (recipeParts.length == 2) {
                    parsingStrategy = new PreparationOnlyStrategy();
                } else if (!StringUtils.stringContainsItemFromList(recipeParts[1].toLowerCase(), new String[]{"person", "rezept", "blech"})) {
                    parsingStrategy = new NoPersonCountParsingStrategy();
                } else {
                    parsingStrategy = new NormalParsingStrategy();
                }

                Map<String, String> recipe = parsingStrategy.getRecipe(recipeParts, lowCarb);
                recipe.put("Mealtype", getMealtype(recipeElement.toString()));

                recipes.add(recipe);
            }
        }
        return recipes;
    }

    public Document getDocument() {
        return document;
    }

    private String getMealtype(String recipe) {
        String[] nonVegetarianIngredients = {
                "fleisch", "fisch", "schinken", "huhn", "hühner", "braunschweiger", "wurst", "schnitzel", "debreziner", "krabben", "speck", "scampi", "shrimps", "garnelen", "forelle", "schwein", "ente", "pute", "rostbraten", "faschiert", "lachs"
        };
        String[] nonVeganIngredients = {
                "eier", "buttermilch", "sauermilch", "sahne", "obers", "käse", "butter", "pudding", "rahm", "cremefine", "creme fraîche", "crème fraîche", "topfen", "mozzarella", "parmesan", "joghurt"
        };
        if (StringUtils.stringContainsItemFromList(recipe.toLowerCase(), nonVegetarianIngredients)) {
            return "";
        } else if (!StringUtils.stringContainsItemFromList(recipe.toLowerCase(), nonVeganIngredients)) {
            return "Vegan";
        } else {
            return "Vegetarian";
        }
    }

    /**
     * Creates a List of all the recipe links on top of the page
     *
     * @return List of id links
     */
    private Map<String, String> getIdLinks() {

        Map<String, String> idLinks = new HashMap<>();
        Elements linkWrapper = document.select("#item6");
        Elements links = linkWrapper.select("a[href]");
        for (Element link : links) {
            if (link.hasText()) {
                idLinks.put(link.text(), link.attr("abs:href"));
            }
        }
        return idLinks;
    }
}
