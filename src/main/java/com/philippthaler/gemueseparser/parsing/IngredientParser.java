package com.philippthaler.gemueseparser.parsing;

import com.philippthaler.gemueseparser.model.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class IngredientParser {
  public static List<Ingredient> parseIngredients(String ingrediensString) {
    List<Ingredient> ingredients = new ArrayList<>();

    String[] ingredientList = ingrediensString.split(",");
    for (int i = 0; i < ingredientList.length; i++) {
      ingredientList[i] = ingredientList[i].trim();
      ingredients.add(parseIngredient(ingredientList[i]));
    }
    return ingredients;
  }

  private static Ingredient parseIngredient(String ingredientString) {
    ingredientString = normalizeMinus(ingredientString);
    ingredientString = removeWhitespaceFromAmount(ingredientString);
    if (ingredientString.matches("([a-zA-ZäöüÄÖÜ])+")) {
      return new Ingredient("", "", ingredientString);
    } else if (ingredientString.matches("[0-9½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒]+ [a-zA-ZäöüÄÖÜ]+")) {
      String[] temp = ingredientString.split(" ");
      return new Ingredient(temp[0], "", temp[1]);
    } else if (ingredientString.matches("[0-9½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒]+ [A-ZÄÖÜ][a-zA-ZäöüÄÖÜ]+ [a-zA-ZäöüÄÖÜ-]+") || ingredientString.matches("[0-9½⅓⅔¼¾⅕⅖⅗⅘⅙⅚⅐⅛⅜⅝⅞⅑⅒]+ [a-zA-ZÄÖÜ]+ [a-zA-ZäöüÄÖÜ-]+")) {
      String[] temp = ingredientString.split(" ");
      return new Ingredient(temp[0], temp[1], temp[2]);
    } else if (ingredientString.matches("(.*[0-9]+ g .*)|(.*dag.*)|(.*EL.*)|(.*TL.*)")) {
      String[] temp = ingredientString.split(" ");
      StringBuilder ingredientMerger = new StringBuilder();
      for (int i = 2; i < temp.length; i++) {
        ingredientMerger.append(temp[i]).append(" ");
      }
      return new Ingredient(temp[0], temp[1], ingredientMerger.toString());
    } else {
      String[] temp = ingredientString.split(" ");
      StringBuilder ingredientMerger = new StringBuilder();
      for (int i = 1; i < temp.length; i++) {
        ingredientMerger.append(temp[i]).append(" ");
      }
      return new Ingredient(temp[0], "", ingredientMerger.toString());
    }
  }

  private static String removeWhitespaceFromAmount(String ingredient) {
    StringBuilder newString = new StringBuilder(ingredient);
    if (ingredient.matches(".*\\d - \\d.*")) {
      newString.deleteCharAt(newString.indexOf("-") - 1);
      newString.deleteCharAt(newString.indexOf("-") + 1);
    }
    return newString.toString();
  }

  private static String normalizeMinus(String ingredient) {
    return ingredient.replaceAll("–", "-");
  }
}
