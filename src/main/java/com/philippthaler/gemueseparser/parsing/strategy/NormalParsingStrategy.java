package com.philippthaler.gemueseparser.parsing.strategy;

import java.util.Map;

public class NormalParsingStrategy implements ParsingStrategy {

  @Override
  public Map<String, String> getRecipe(String[] recipeParts, String lowCarb) {
    String title = recipeParts[0];
    String personCount = recipeParts[1];
    String ingredients = recipeParts[2];
    StringBuilder preparation = new StringBuilder();
    for (int i = 3; i < recipeParts.length; i++) {
      preparation.append(recipeParts[i]).append("\n");
    }

    return createRecipeMap(title, personCount, ingredients, preparation.toString(), lowCarb);
  }
}
