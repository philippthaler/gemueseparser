package com.philippthaler.gemueseparser.parsing.strategy;

import java.util.HashMap;
import java.util.Map;

public interface ParsingStrategy {
  Map<String, String> getRecipe(String[] recipeParts, String lowCarb);

  /**
   * Helper method that creates and returns the recipe as a map
   *
   * @param title       The title of the recipe
   * @param personCount For how many people this recipe is
   * @param ingredients The ingredients
   * @param preparation The preparation
   * @param lowCarb     Shows if it's low carb, as marked on some recipes
   * @return A map representing a recipe
   */
  default Map<String, String> createRecipeMap(String title, String personCount, String ingredients, String preparation, String lowCarb) {
    Map<String, String> recipe = new HashMap<>();
    recipe.put("Title", title);
    recipe.put("PersonCount", personCount);
    recipe.put("Ingredients", ingredients);
    recipe.put("Preparation", preparation);
    recipe.put("LowCarb", lowCarb);
    return recipe;
  }
}
