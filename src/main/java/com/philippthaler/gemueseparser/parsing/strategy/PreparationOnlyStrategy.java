package com.philippthaler.gemueseparser.parsing.strategy;

import java.util.Map;

public class PreparationOnlyStrategy implements ParsingStrategy{
  @Override
  public Map<String, String> getRecipe(String[] recipeParts, String lowCarb) {
    String title = recipeParts[0];
    String preparation = recipeParts[1];
    return createRecipeMap(title, "", "", preparation, lowCarb);
  }
}
