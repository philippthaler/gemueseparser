package com.philippthaler.gemueseparser.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class for saving a recipe
 */
public class Recipe {
  private final String title;
  private final String personCount;
  private final String preparation;
  private final String lowCarb;
  private final String mealtype;
  private final List<String> ingredients;
  private Set<String> categories;
  private Set<String> keywords;

  public Recipe(String title, String personCount, List<String> ingredients, String preparation, String lowCarb, String mealtype) {
    this.title = title;
    this.personCount = personCount;
    this.preparation = preparation;
    this.ingredients = ingredients;
    this.lowCarb = lowCarb;
    this.mealtype = mealtype;
    categories = new HashSet<>();
    keywords = new HashSet<>();
  }

  public String getTitle() {
    return title;
  }

  public String getPersonCount() {
    return personCount;
  }

  public String getPreparation() {
    return preparation;
  }

  public String getLowCarb() {
    return lowCarb;
  }

  public List<String> getIngredients() {
    return ingredients;
  }

  public Set<String> getCategories() {
    return categories;
  }

  public void addCategory(String category) {
    categories.add(category);
  }

  public void addKeyword(String keyword) {
    keywords.add(keyword);
  }

  public void setKeywords(Set<String> keywords) {
    this.keywords = keywords;
  }

  @Override
  public String toString() {
    return String.format("%s\n\tPersonen: %s\n\tZutaten: %s\n\tZubereitung: %s", title, personCount, ingredients, preparation);
  }
}
