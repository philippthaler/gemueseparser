package com.philippthaler.gemueseparser.json;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Class for writing JSON to a file
 */
public class JsonWriter {

  /**
   *
   * @param outputPath The Path, where the file should be saved
   * @param jsonString The JSON as a String
   * @throws IOException
   */
  public static void saveJson(Path outputPath, String jsonString) throws IOException {
    if (!Files.exists(outputPath)) {
      Files.createDirectory(outputPath);
    }
    String filename = outputPath.toString() + "/recipes.json";

    Writer writer = new FileWriter(outputPath.toString() + "/recipes.json");
    writer.write(jsonString);
    writer.close();
    if(Files.exists(Paths.get(filename))) {
      System.out.println("File saved at: " + Paths.get(filename).toAbsolutePath());
    }
  }
}
