package com.philippthaler.gemueseparser.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * Class to generate JSON
 */
public class JsonGenerator {

  /**
   *
   * @param recipes
   * @return Returns the JSON String
   */
  public static String generateJson(Object recipes) {
    GsonBuilder builder = new GsonBuilder();
    builder.setPrettyPrinting();
    Gson gson = builder.create();

    return gson.toJson(recipes);
  }
}
