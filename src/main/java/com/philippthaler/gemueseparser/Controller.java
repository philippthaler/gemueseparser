package com.philippthaler.gemueseparser;

import com.philippthaler.gemueseparser.json.JsonGenerator;
import com.philippthaler.gemueseparser.json.JsonWriter;
import com.philippthaler.gemueseparser.parsing.*;
import com.philippthaler.gemueseparser.model.Recipe;
import com.philippthaler.gemueseparser.util.KeywordGenerator;
import com.philippthaler.gemueseparser.util.LinkGetter;
import com.philippthaler.gemueseparser.util.RecipeLinkGetter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Controller class that gets all the links, parses the recipes and saves them to JSON
 */
public class Controller {

  private static List<String> secondPageList;

  private final String RECIPELINK = "https://tiroler.gemuesekiste.at/rezepte.html";
  // Recipes get saved in a Map<RecipeTitle, Recipe>
  private final Map<String, Recipe> recipes;
  private Map<String, String> categoryLinks;
  private KeywordGenerator keywordGenerator;

  public Controller() {
    recipes = new HashMap<>();
    secondPageList = Arrays.asList("Bohnen", "Broccoli", "Karotte", "Kartoffel", "Kohlrabi", "Tomate", "Zucchini");
    keywordGenerator = new KeywordGenerator();
  }

  protected void start() {
    // Saves the Recipes in a map
    saveRecipes();

    try {
      JsonWriter.saveJson(Paths.get("./output"), JsonGenerator.generateJson(recipes));
    } catch (IOException e) {
      System.err.println("The file couldn't be saved");
    }
    System.out.println(recipes.size() + " Recipes saved");
  }

  private void saveRecipes() {
    // First get all the links
    LinkGetter recipeLinkGetter;
    try {
      recipeLinkGetter = new RecipeLinkGetter(RECIPELINK);
      categoryLinks = recipeLinkGetter.getLinks();
    } catch (IOException e) {
      System.err.println("URL not found");
    }
    try {
      for (String category : categoryLinks.keySet()) {
        // For every URL, parse the recipes
        RecipeParser parser = new GemueseParser(categoryLinks.get(category), "item13");
        startParser(parser, category);
      }
      for (String category : secondPageList) {
        RecipeParser parser = new GemueseParser(getSecondPageUrl(category));
        startParser(parser, category);
      }
    } catch (IOException e) {
      System.err.println("The element couldn't be found");
    }
  }

  /**
   * Makes a List out of all the ingredients
   * Removes leading and trailing whitespace
   *
   * @param ingredients A String of ingredients
   * @return A list of ingredients
   */
  private List<String> getIngredientList(String ingredients) {
    String[] ingredientList = ingredients.split(",");
    for (int i = 0; i < ingredientList.length; i++) {
      ingredientList[i] = ingredientList[i].trim();
      if(ingredientList[i].contains(":")) {
        System.out.println(ingredientList[i]);
      }
    }
    return new ArrayList<>(Arrays.asList(ingredientList));
  }

  private void startParser(RecipeParser parser, String category) {
    for (Map<String, String> recipe : parser.parse()) {
      // If the recipe is already saved, only add the category
      if (recipes.containsKey(recipe.get("Title"))) {
        recipes.get(recipe.get("Title")).addCategory(category);
      } else {
        // Adding the recipe to the map
        Recipe newRecipe = new Recipe(recipe.get("Title"), recipe.get("PersonCount"),
            getIngredientList(recipe.get("Ingredients")), recipe.get("Preparation"), recipe.get("LowCarb"), recipe.get("Mealtype"));
        newRecipe.addCategory(category);
        newRecipe.setKeywords(keywordGenerator.generateKeywords(recipe.get("Title")));
        recipes.put(recipe.get("Title"), newRecipe);
      }
    }
  }

  private String getSecondPageUrl(String category) {
    return "http://tiroler.gemuesekiste.at/tgk/rezepte" + category.toLowerCase() + "2.html";
  }

}


